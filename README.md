# AMUSTserver

Emulation of the long gone server for Auto Modellista US Tuned (Playstation 2)

This project is based on the Biohazard Outbreak servers and was created for preservation purposes.

This was lying around my hard drive for years now, I have no idea if it is complete. Expect bugs and glitches, you've been warned ;)

## Disclaimer

This project is not affiliated wth Capcom. 
AMUSTserver is meant for use with authentic and licensed Playstation 2 hardware and software. It should not be used to attempt to facilitate copyright infringment.
The developers and authors of this Software cannot be held liable for your use or misuse of this software.
